module.exports = {
  skipFiles: [
    '@chainlink',
    '@openzeppelin',
    '@uma',
    'contracts/test/',
    'contracts/common/',
  ],
  measureStatementCoverage: false,
};
